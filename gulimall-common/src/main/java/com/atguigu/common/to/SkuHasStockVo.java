package com.atguigu.common.to;

import lombok.Data;

/**
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-22 15:16:58
 */

@Data
public class SkuHasStockVo {

    private Long skuId;

    private Boolean hasStock;

}
