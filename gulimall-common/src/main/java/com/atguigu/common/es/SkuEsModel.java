package com.atguigu.common.es;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-22 15:16:58
 */

@Data
public class SkuEsModel {

    private Long skuId;

    private Long spuId;

    private String skuTitle;

    private BigDecimal skuPrice;

    private String skuImg;

    /**
     * 销量
     */
    private Long saleCount;

    /**
     * 是否拥有库存
     */
    private Boolean hasStock;

    /**
     * 热度评分
     */
    private Long hotScore;

    /**
     * 品牌id
     */
    private Long brandId;

    /**
     * 分类id
     */
    private Long catalogId;

    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 品牌图片
     */
    private String brandImg;

    /**
     * 分类名称
     */
    private String catalogName;

    /**
     * 属性
     */
    private List<Attrs> attrs;

    @Data
    public static class Attrs {

        private Long attrId;

        private String attrName;

        private String attrValue;

    }


}
