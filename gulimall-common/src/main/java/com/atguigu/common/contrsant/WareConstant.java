package com.atguigu.common.contrsant;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class WareConstant {

    public static final String STOCK_EVENT_EXCHANGE = "stock-event-exchange";    // 交换机

    public static final String STOCK_DELAY_QUEUE = "stock.delay.queue";  // 队列1  延时队列
    public static final String STOCK_RELEASE_STOCK_QUEUE = "stock.release.stock.queue";  // 队列2  信死了（过期时间到了），消息去的队列

    public static final String STOCK_LOCKED_ROUTING_KEY = "stock.locked";    // 路由键1 库存锁定
    public static final String STOCK_RELEASE_ROUTING_KEY = "stock.release";  // 路由键2 信死了（过期时间到了），用的路由键是什么

    public static final String STOCK_RELEASE_BINDING = "stock.release.#";    // 绑定关系2

    public static final Long MESSAGE_TTL = 120000L;  // 消息等待时间

    @Getter
    @AllArgsConstructor
    public enum LockStatusEnum {
        LOCKED(1, "已锁定"),
        UNLOCKED(2, "已解锁"),
        DEDUCTED(3, "已扣减");

        private int code;
        private String message;
    }

    public enum  PurchaseStatusEnum{
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),
        FINISH(3,"已完成"),
        HASERROR(4,"有异常");
        private int code;
        private String msg;

        PurchaseStatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }


    public enum  PurchaseDetailStatusEnum{
        CREATED(0,"新建"),ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),FINISH(3,"已完成"),
        HASERROR(4,"采购失败");
        private int code;
        private String msg;

        PurchaseDetailStatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
