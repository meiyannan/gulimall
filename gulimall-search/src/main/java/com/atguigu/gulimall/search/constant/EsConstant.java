package com.atguigu.gulimall.search.constant;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: meiyn
 * @createTime: 2021-11-11 16:48
 **/
public class EsConstant {

    //在es中的索引
    public static final String PRODUCT_INDEX = "mall_product";

    public static final Integer PRODUCT_PAGESIZE = 16;
}
