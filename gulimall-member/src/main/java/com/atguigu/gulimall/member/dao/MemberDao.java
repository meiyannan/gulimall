package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-23 15:10:12
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
