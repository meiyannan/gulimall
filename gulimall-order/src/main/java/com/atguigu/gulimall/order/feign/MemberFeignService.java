package com.atguigu.gulimall.order.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author meiyannan
 * @date   2021/7/16 23:31
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {
    /**
     * 根据会员id获取地址信息
     * @param memberId
     * @return
     */
    @GetMapping("/member/memberreceiveaddress/address/{memberId}")
    R getAddress(@PathVariable("memberId") Long memberId);

    /**
     * 根据地址id查询地址信息
     * @param id
     * @return
     */
    @GetMapping("/member/memberreceiveaddress/info/{id}")
    R getReceiveAddressInfo(@PathVariable("id") Long id);
}
