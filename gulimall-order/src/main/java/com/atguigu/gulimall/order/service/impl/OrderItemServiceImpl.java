package com.atguigu.gulimall.order.service.impl;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.order.dao.OrderItemDao;
import com.atguigu.gulimall.order.entity.OrderItemEntity;
import com.atguigu.gulimall.order.service.OrderItemService;


@Service("orderItemService")
@RabbitListener(queues = {"hello-java-queue"})
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<OrderItemEntity> listByOrderSn(String orderSn) {
        return this.list(new QueryWrapper<OrderItemEntity>().eq("order_sn", orderSn));
    }

    /**
     * queues：声明需要监听的队列
     * channel：当前传输数据的通道
     */
//    @RabbitListener(queues = {"hello-java-queue"})
    @SneakyThrows
    @RabbitHandler
    public void revieveMessage(Message message,
                               OrderReturnReasonEntity content,Channel channel) {
        //拿到主体内容
        byte[] body = message.getBody();
        //拿到的消息头属性信息
        MessageProperties messageProperties = message.getMessageProperties();
        System.out.println("接受到的消息...内容" + message + "===内容：" + content);

        //channel内按顺序自增的
        long deliveryTag = messageProperties.getDeliveryTag();
        System.out.println("deliveryTag:"+deliveryTag);

        try{
            //手动签收ack 非批量模式 multiple：是否批量签收
            channel.basicAck(deliveryTag,false);
            System.out.println("签收了的货物------"+deliveryTag);
        }catch (Exception e){
            //网络中断
            //requeue:ture  消息发回服务器，服务器重新入队
            channel.basicNack(deliveryTag,false,true);
            log.error("网络中断");
        }

    }

    @RabbitHandler
    public void revieveMessage(Message message,
                               OrderEntity orderEntity) {
        //拿到主体内容
        byte[] body = message.getBody();
        //拿到的消息头属性信息
        MessageProperties messageProperties = message.getMessageProperties();
        System.out.println("接受到的消息...内容" + message + "===内容：" + orderEntity);

    }

}