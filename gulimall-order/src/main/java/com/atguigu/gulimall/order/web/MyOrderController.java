package com.atguigu.gulimall.order.web;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.order.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 我的订单
 *
 * @author UnityAlvin
 * @date 2021/7/25 21:44
 */
@Controller
public class MyOrderController {
    @Resource
    private OrderService orderService;

    @GetMapping("/my-order.html")
    public String orderList(@RequestParam(value = "pageNum", defaultValue = "1") String pageNum,
                            @RequestParam(value = "pageNum", defaultValue = "5") String limit,
                            Model model) {
        Map<String ,Object> pages = new HashMap<>();
        pages.put("page",pageNum);
        pages.put("limit",limit);
        PageUtils orderPages = orderService.queryOrderPage(pages);
        model.addAttribute("orderPages",orderPages);
        return "my-order";
    }
//    @GetMapping("/{page}.html")
//    public String pageConver(@PathVariable("page") String page) {
//
//        return page;
//    }

}
