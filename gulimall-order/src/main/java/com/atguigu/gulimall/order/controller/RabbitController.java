package com.atguigu.gulimall.order.controller;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * @author Administrator
 * @create 2022/2/16 17:04
 */
@Slf4j
@RestController
public class RabbitController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/sendMsg")
    public String sendMsg(@RequestParam(value = "num",defaultValue = "10") Integer num){
        for(int i=0;i<=num;i++){
            if(i%2 ==0){
                OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
                reasonEntity.setId(1L);
                reasonEntity.setCreateTime(new Date());
                reasonEntity.setName("reason+哈哈");
                reasonEntity.setStatus(1);
                reasonEntity.setSort(2);
                rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",
                        reasonEntity,new CorrelationData(UUID.randomUUID().toString()));
                log.info("消息发送完成:{}",reasonEntity);
            }else{
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setId(2L);
                orderEntity.setMemberUsername("张三");
                rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",
                        orderEntity,new CorrelationData(UUID.randomUUID().toString()));
                log.info("消息发送完成:{}",orderEntity);
            }
        }
        return "111";
    }
}
