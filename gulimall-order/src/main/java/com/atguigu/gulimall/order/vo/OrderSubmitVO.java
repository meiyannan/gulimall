package com.atguigu.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 点击提交订单，页面传过来的数据
 * @author meiyn
 * @date 2021/7/18 8:58
 */
@Data
public class OrderSubmitVO {
    private Long addrId; //收获地址id
    private String orderToken; //防重令牌
    private BigDecimal payPrice; //应付价格 验价

    //用户信息 直接从session中取
}
