package com.atguigu.gulimall.order.feign;

import com.atguigu.common.dto.WareLockStockDTO;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author meiyannan
 * @date 2021/7/17 15:48
 */
@FeignClient("gulimall-ware")
public interface WareFeignService {

    /**
     * 查询sku是否有库存
     * @return
     */
    @PostMapping("/ware/waresku/hasStock")
    R stockOrNot(@RequestBody List<Long> skuIds);

    /**
     * 为下单商品锁定库存
     * @param wareLockStockDTO
     * @return
     */
    @PostMapping("/ware/waresku/lockStock")
    R lockStock(@RequestBody WareLockStockDTO wareLockStockDTO);
}
