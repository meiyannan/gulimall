package com.atguigu.gulimall.order.service;

import com.atguigu.common.dto.mq.SeckillSkuDTO;
import com.atguigu.gulimall.order.vo.*;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.order.entity.OrderEntity;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-23 15:20:53
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取订单确认页的数据
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    OrderConfirmVO getConfirmInfo() throws ExecutionException, InterruptedException;

    /**
     * 提交订单
     * @param orderSubmitVO
     * @return
     */
    OrderSubmitResponseVO submitOrder(OrderSubmitVO orderSubmitVO);

    /**
     * 根据订单编号获取订单信息
     * @param orderSn
     * @return
     */
    OrderEntity getOrderByOrderSn(String orderSn);

    /**
     * 关闭订单
     * @param order
     */
    void closeOrder(OrderEntity order);

    /**
     * 根据订单编号获取支付所需要的信息
     * @param orderSn
     * @return
     */
    PayVO getOrderPayInfo(String orderSn);

    /**
     * 根据页数查询订单列表
     * @param params
     * @return
     */
    PageUtils queryOrderPage(Map<String, Object> params);

    /**
     * 处理支付宝的回调数据
     * @param payAsyncVO
     * @return
     */
    String handlePayResult(PayAsyncVO payAsyncVO);

    /**
     * 根据订单编号修改指定状态
     * @param orderSn
     * @param status
     */
    void updateOrderStatus(String orderSn, int status);

    /**
     * 创建秒杀订单
     * @param seckillSkuDTO
     */
    void createSeckillOrder(SeckillSkuDTO seckillSkuDTO);


    /**
     * 关闭秒杀订单
     * @param seckillSkuDTO
     */
    void closeSeckillOrder(SeckillSkuDTO seckillSkuDTO);

    /**
     * 获取秒杀订单的数据
     * @param seckillSkuDTO
     * @return
     */
    SeckillOrderConfirmVO getSeckillOrderInfo(SeckillSkuDTO seckillSkuDTO) throws ExecutionException, InterruptedException;

    /**
     * 提交秒杀订单
     * @param seckillOrderSubmitVO
     */
    OrderSubmitResponseVO submitSeckillOrder(SeckillOrderSubmitVO seckillOrderSubmitVO);
}

