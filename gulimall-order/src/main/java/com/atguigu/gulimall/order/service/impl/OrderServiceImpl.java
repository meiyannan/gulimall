package com.atguigu.gulimall.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.contrsant.OrderConstant;
import com.atguigu.common.contrsant.SeckillConstant;
import com.atguigu.common.dto.OrderDTO;
import com.atguigu.common.dto.SeckillSkuRedisDTO;
import com.atguigu.common.dto.WareLockStockDTO;
import com.atguigu.common.dto.WareLockStockDTO.OrderItemDTO;
import com.atguigu.common.dto.mq.SeckillSkuDTO;
import com.atguigu.common.exception.Assert;
import com.atguigu.common.exception.BizCodeEnume;
import com.atguigu.common.exception.BusinessException;
import com.atguigu.common.to.SkuHasStockVo;
import com.atguigu.common.to.SkuStockTO;
import com.atguigu.common.utils.R;
import com.atguigu.common.vo.MemberResponseVo;
import com.atguigu.gulimall.order.dao.OrderItemDao;
import com.atguigu.gulimall.order.dto.MemberReceiveAddressDTO;
import com.atguigu.gulimall.order.dto.OrderCreateDTO;
import com.atguigu.gulimall.order.dto.SpuInfoDTO;
import com.atguigu.gulimall.order.entity.OrderItemEntity;
import com.atguigu.gulimall.order.entity.PaymentInfoEntity;
import com.atguigu.gulimall.order.feign.CartFeignService;
import com.atguigu.gulimall.order.feign.MemberFeignService;
import com.atguigu.gulimall.order.feign.ProductFeignService;
import com.atguigu.gulimall.order.feign.WareFeignService;
import com.atguigu.gulimall.order.interceptor.LoginUserInterceptor;
import com.atguigu.gulimall.order.service.OrderItemService;
import com.atguigu.gulimall.order.service.PaymentInfoService;
import com.atguigu.gulimall.order.vo.*;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.BeanUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.order.dao.OrderDao;
import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.service.OrderService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    private ThreadLocal<OrderSubmitVO> threadLocal = new ThreadLocal<>();

    @Resource
    private MemberFeignService memberFeignService;

    @Resource
    private CartFeignService cartFeignService;

    @Resource
    private WareFeignService wareFeignService;

    @Resource
    private ProductFeignService productFeignService;

    @Resource
    private OrderItemService orderItemService;

    @Resource
    private PaymentInfoService paymentInfoService;
    @Resource
    private OrderItemDao orderItemDao;

    @Resource
    private ThreadPoolExecutor executor;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 获取订单确认页的数据
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Override
    public OrderConfirmVO getConfirmInfo() throws ExecutionException, InterruptedException {
        OrderConfirmVO orderConfirmVO = new OrderConfirmVO();
        MemberResponseVo memberTO = LoginUserInterceptor.threadLocal.get();
        // 共享主线程中拿到原请求数据
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 1.根据会员id查询地址
        CompletableFuture<Void> memberFuture = CompletableFuture.runAsync(() -> {

            // 重新设置异步任务的请求数据
            RequestContextHolder.setRequestAttributes(requestAttributes);
            R r = memberFeignService.getAddress(memberTO.getId());
            if (r.getCode() == 0) {
                List<OrderConfirmVO.MemberAddressVO> address = r.getData(new TypeReference<List<OrderConfirmVO.MemberAddressVO>>() {
                });
                orderConfirmVO.setAddress(address);
            }
        }, executor);

        // 2.获取当前用户的所有选中购物项
        CompletableFuture<Void> cartFuture = CompletableFuture.runAsync(() -> {
            // 重新设置异步任务的请求数据
            RequestContextHolder.setRequestAttributes(requestAttributes);
            List<OrderConfirmVO.OrderItemVO> orderItems = cartFeignService.getCurrentUserCartItems();
            orderConfirmVO.setItems(orderItems);

        }, executor).thenRunAsync(() -> {
            // 3.查询所有购物车商品的库存信息
            List<OrderConfirmVO.OrderItemVO> items = orderConfirmVO.getItems();
            if(CollectionUtils.isNotEmpty(items)){
                List<Long> skuIds = items.stream().map(OrderConfirmVO.OrderItemVO::getSkuId).collect(Collectors.toList());
                R r = wareFeignService.stockOrNot(skuIds);
                if (r.getCode() == 0) {
                    List<SkuHasStockVo> SkuStockTOs = r.getData(new TypeReference<List<SkuHasStockVo>>() {
                    });
                    Map<Long, Boolean> stocks = SkuStockTOs.stream().collect(Collectors.toMap(SkuHasStockVo::getSkuId,
                            SkuHasStockVo::getHasStock));
                    orderConfirmVO.setStocks(stocks);
                }
            }

        }, executor);

        CompletableFuture.allOf(memberFuture, cartFuture).get();

        // 4.设置用户积分
        orderConfirmVO.setIntegration(memberTO.getIntegration());

        // 5.计算应付、总付总额
        setAggs(orderConfirmVO);

        // TODO 6.防重令牌
        String orderToken = UUID.randomUUID().toString().replace("-", "");
        orderConfirmVO.setOrderToken(orderToken);
        stringRedisTemplate.opsForValue().set(OrderConstant.ORDER_TOKEN_PREFIX + memberTO.getId(), orderToken, 30,
                TimeUnit.MINUTES);

        return orderConfirmVO;
    }


    /**
     * 提交订单
     * @param orderSubmitVO
     * @return
     */
    @Override
    @Transactional
//    @GlobalTransactional
    public OrderSubmitResponseVO submitOrder(OrderSubmitVO orderSubmitVO) {
        threadLocal.set(orderSubmitVO);
        String orderToken = orderSubmitVO.getOrderToken();
        OrderSubmitResponseVO responseVO = new OrderSubmitResponseVO();
        MemberResponseVo memberTO = LoginUserInterceptor.threadLocal.get();
        //Token 获取、比较和删除必须是原子性 在redis 使用 lua 脚本完成这个操作
        // 这段脚本的意思是，如果获取key对应的value是传过来的值，那就调用删除方法返回1，否则返回0
        String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        // 原子删锁
        /*
            实现RedisScript接口的实现类(脚本内容,执行完之后的返回值类型)
            一个存key的list
            要对比的value
         */
        Long result = stringRedisTemplate.execute(new DefaultRedisScript<>(script, Long.class),
                Arrays.asList(OrderConstant.ORDER_TOKEN_PREFIX + memberTO.getId()), orderToken);
        // 1、校验令牌
        if (result == 0L) {
            // 验证失败
            responseVO.setCode(1);
        } else {
            // 验证成功
            // 2、创建订单、订单项等信息
            OrderCreateDTO orderCreateDTO = createOrder();

            // 3、比价
            // 考虑到最终金额可能会有多位小数的问题，此处选择算差价
            BigDecimal payAmount = orderCreateDTO.getOrder().getPayAmount();
            BigDecimal payPrice = orderSubmitVO.getPayPrice();
            if (Math.abs(payAmount.subtract(payPrice).doubleValue()) < 0.01) {
                // 对比成功
                // 4、将准备好的订单信息保存到数据库
                saveOrderAndItems(orderCreateDTO);

                // 5、库存锁定
                WareLockStockDTO wareLockStockDTO = new WareLockStockDTO();
                wareLockStockDTO.setOrderSn(orderCreateDTO.getOrder().getOrderSn());
                List<OrderItemDTO> orderItemDTOs = orderCreateDTO.getOrderItems().stream().map(orderItem ->{
                            OrderItemDTO itemDto = new OrderItemDTO();
                            itemDto.setSkuId(orderItem.getSkuId());
                            itemDto.setSkuName(orderItem.getSkuName());
                            itemDto.setSkuQuantity(orderItem.getSkuQuantity());
                            return itemDto;
                        }
                ).collect(Collectors.toList());
                wareLockStockDTO.setItems(orderItemDTOs);

                //为了保证高并发，可以发消息给库存服务，库存服务自己回滚。
                //库存服务也可以使用自动解锁模式-使用消息队列
                R r = wareFeignService.lockStock(wareLockStockDTO);
                responseVO.setOrder(orderCreateDTO.getOrder());
                Assert.isTrue(r.getCode() == 0, BizCodeEnume.HAS_STOCK_EXCEPTION);
                responseVO.setCode(0);

                // 库存全部锁定成功，可以支付了
                // 测试库存自动解锁，往外抛个异常
//                throw new BusinessException(BizCodeEnume.UNKNOW_EXCEPTION);

                // 给MQ发消息，让它定时取消订单
                rabbitTemplate.convertAndSend(OrderConstant.ORDER_EVENT_EXCHANGE,
                        OrderConstant.ORDER_CREATE_ORDER_ROUTING_KEY, orderCreateDTO.getOrder());
            } else {
                // 金额对比失败
                responseVO.setCode(2);
            }
        }
        return responseVO;
    }

    @Override
    public OrderEntity getOrderByOrderSn(String orderSn) {
        return this.getOne(new QueryWrapper<OrderEntity>().eq("order_sn", orderSn));
    }


    /**
     * 关闭订单
     * @param order
     */
    @Override
    @Transactional
    public void closeOrder(OrderEntity order) {
        // 去数据库查询最新的订单信息
        OrderEntity currentOrder = this.getById(order.getId());

        // 如果是未付款的订单，就可以取消
        if (currentOrder.getStatus() == OrderConstant.OrderStatusEnum.TO_BE_PAID.getCode()) {
            OrderEntity newOrder = new OrderEntity();
            newOrder.setId(order.getId());
            newOrder.setStatus(OrderConstant.OrderStatusEnum.CANCELLED.getCode());
            this.updateById(newOrder);
//            OrderDTO orderDTO = OrderCovertBasic.INSTANCE.orderToOrderDTO(currentOrder);
            OrderDTO orderDTO = new OrderDTO();
            org.springframework.beans.BeanUtils.copyProperties(currentOrder,orderDTO);

            /**
             * 场景：下单时，订单调用库存成功，但是由于订单系统卡顿，网络等原因，订单卡住了
             *      假如卡了两分钟，这时候库存服务去自动解锁库存，发现订单还是创建状态，就没有解锁库存；
             *      但是订单服务恢复后，这单要关单，所以最终，库存永远不会解锁。
             *   解决：订单关闭后，也主动发送消息（路由键需要跟库存的交换机绑定，发送到库存队列）
             */
            try {
                rabbitTemplate.convertAndSend(OrderConstant.ORDER_EVENT_EXCHANGE, OrderConstant.ORDER_RELEASE_OTHER_ROUTING_KEY,
                        orderDTO);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    /**
     * 根据订单编号获取支付所需要的信息
     * @param orderSn
     * @return
     */
    @Override
    public PayVO getOrderPayInfo(String orderSn) {
        OrderEntity order = getOrderByOrderSn(orderSn);
        PayVO payVO = new PayVO();
        payVO.setOutTradeNo(orderSn); //商户订单号
        List<OrderItemEntity> orderItems = orderItemService.listByOrderSn(orderSn);
        payVO.setSubject(orderItems.get(0).getSkuName()); // 订单名称
        // 付款金额 必填
        payVO.setTotalAmount(order.getPayAmount().setScale(2, BigDecimal.ROUND_UP).toString());
        payVO.setBody(orderItems.get(0).getSkuAttrsVals());// 商品描述 可空

        return payVO;
    }

    /**
     * 根据页数查询订单列表
     * @param params
     * @return
     */
    @Override
    public PageUtils queryOrderPage(Map<String, Object> params) {
        // 获取当前登录用户的信息
        MemberResponseVo memberTO = LoginUserInterceptor.threadLocal.get();

        // 分页查询当前用户的所有订单
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>().eq("member_id", memberTO.getId()).orderByDesc("id")
        );

        // 查询每个订单的订单项
        List<OrderEntity> orders = page.getRecords().stream().map(order -> {
            List<OrderItemEntity> orderItems = orderItemService.listByOrderSn(order.getOrderSn());
            order.setOrderItems(orderItems);
            return order;
        }).collect(Collectors.toList());

        page.setRecords(orders);
        return new PageUtils(page);
    }

    /**
     * 处理支付宝的回调数据
     * @param payAsyncVO
     * @return
     */
    @Override
    public String handlePayResult(PayAsyncVO payAsyncVO) {
        String tradeStatus = payAsyncVO.getTrade_status();

        // 保存交易流水
        PaymentInfoEntity paymentInfo = new PaymentInfoEntity();
        paymentInfo.setOrderSn(payAsyncVO.getOut_trade_no());
        paymentInfo.setAlipayTradeNo(payAsyncVO.getTrade_no());
        paymentInfo.setPaymentStatus(tradeStatus);
        paymentInfo.setCallbackTime(payAsyncVO.getNotify_time());
        paymentInfoService.save(paymentInfo);

        if (tradeStatus.equals("TRADE_SUCCESS") || tradeStatus.equals("TRADE_FINISHED")) {
            // 修改订单状态
            this.updateOrderStatus(payAsyncVO.getOut_trade_no(), OrderConstant.OrderStatusEnum.PAID.getCode());
        }
        return "success";
    }

    /**
     * 根据订单编号修改指定状态
     * @param orderSn
     * @param status
     */
    @Override
    public void updateOrderStatus(String orderSn, int status) {
        this.update(new UpdateWrapper<OrderEntity>().set("status", status).eq("order_sn", orderSn));
    }

    @Override
    public void createSeckillOrder(SeckillSkuDTO seckillSkuDTO) {
        //构建秒杀订单基本数据
        OrderEntity order = buildSeckillOrder(seckillSkuDTO.getOrderSn());
        BigDecimal totalPrice = seckillSkuDTO.getPrice().multiply(new BigDecimal(seckillSkuDTO.getNum()));
        order.setTotalAmount(totalPrice);
        // 计算运费，如果单笔订单金额大于等于包邮金额，则免运费，否则运费8元
        order.setFreightAmount(totalPrice.compareTo(OrderConstant.FREE_FREIGHT_PRICE) >= 0 ? BigDecimal.ZERO
                : OrderConstant.FREIGHT);
        order.setPayAmount(order.getTotalAmount().add(order.getFreightAmount()));
        order.setMemberId(seckillSkuDTO.getMemberId());
        order.setIntegration(totalPrice.intValue());
        order.setGrowth(totalPrice.intValue());

        //构建秒杀订单购物项
        OrderItemEntity orderItem = buildSeckillOrderItem(seckillSkuDTO);
        this.save(order);
        orderItemService.save(orderItem);

        // 超时未支付，需要取消秒杀订单
        rabbitTemplate.convertAndSend(OrderConstant.ORDER_EVENT_EXCHANGE,
                OrderConstant.ORDER_CREATE_ORDER_ROUTING_KEY, seckillSkuDTO);
    }

    @Override
    public void closeSeckillOrder(SeckillSkuDTO seckillSkuDTO) {
        // 去数据库查询最新的订单信息
        OrderEntity currentOrder = this.getOrderByOrderSn(seckillSkuDTO.getOrderSn());

        // 如果是未付款的订单，就可以取消
        if (currentOrder.getStatus() == OrderConstant.OrderStatusEnum.TO_BE_PAID.getCode()) {
            OrderEntity newOrder = new OrderEntity();
            newOrder.setId(currentOrder.getId());
            newOrder.setStatus(OrderConstant.OrderStatusEnum.CANCELLED.getCode());
            this.updateById(newOrder);
            rabbitTemplate.convertAndSend(OrderConstant.ORDER_EVENT_EXCHANGE,
                    OrderConstant.ORDER_SECKILL_RELEASE_OTHER_ROUTING_KEY,
                    seckillSkuDTO);
        }
    }

    /**
     * 获取秒杀订单的数据
     * @param seckillSkuDTO
     * @return
     */
    @Override
    public SeckillOrderConfirmVO getSeckillOrderInfo(SeckillSkuDTO seckillSkuDTO) {
        MemberResponseVo memberTO = LoginUserInterceptor.threadLocal.get();
        StringBuilder strBuilder = new StringBuilder(SeckillConstant.SECKILL_ABSENT_PREFIX);
        strBuilder.append(memberTO.getId()).append("_").append(seckillSkuDTO.getPromotionSessionId()).append("_")
                .append(seckillSkuDTO.getSkuId());
        String value = stringRedisTemplate.opsForValue().get(strBuilder.toString());
        // 当前用户根本就没抢到商品
        if (org.apache.commons.lang.StringUtils.isEmpty(value)) {
            return null;
        }
        // 从缓存中拿秒杀商品的基础信息
        String redisKey = seckillSkuDTO.getPromotionSessionId() + "_" + seckillSkuDTO.getSkuId();
        BoundHashOperations<String, String, String> skusOperations = stringRedisTemplate.boundHashOps(SeckillConstant
                .SECKILL_SKU_PREFIX);
        String jsonStr = skusOperations.get(redisKey);
        SeckillSkuRedisDTO seckillSkuRedisDTO = JSON.parseObject(jsonStr, SeckillSkuRedisDTO.class);

        if (seckillSkuDTO.getNum() > seckillSkuRedisDTO.getSeckillSkuRelationDTO().getSeckillLimit()) {
            return null;
        }

        // 准备要显示给用户的秒杀数据
        SeckillOrderConfirmVO seckillConfirmVO = new SeckillOrderConfirmVO();

        // 根据会员id查询地址
        R r = memberFeignService.getAddress(memberTO.getId());
        if (r.getCode() == 0) {
            List<SeckillOrderConfirmVO.MemberAddressVO> address =
                    r.getData(new TypeReference<List<SeckillOrderConfirmVO.MemberAddressVO>>() {
                    });
            seckillConfirmVO.setAddress(address);
        }
        seckillConfirmVO.setOrderSn(seckillSkuDTO.getOrderSn());
        seckillConfirmVO.setTitle(seckillSkuRedisDTO.getSkuInfoDTO().getSkuTitle());
        seckillConfirmVO.setImage(seckillSkuRedisDTO.getSkuInfoDTO().getSkuDefaultImg());
        seckillConfirmVO.setPrice(seckillSkuRedisDTO.getSeckillSkuRelationDTO().getSeckillPrice());
        seckillConfirmVO.setNum(seckillSkuDTO.getNum());
        seckillConfirmVO.setPromotionSessionId(seckillSkuDTO.getPromotionSessionId());
        seckillConfirmVO.setSkuId(seckillSkuDTO.getSkuId());
        seckillConfirmVO.setSkuAttr(seckillSkuRedisDTO.getSkuAttr());

//        // 设置用户积分
        seckillConfirmVO.setIntegration(memberTO.getIntegration());
//
//        // 计算应付、总付
        BigDecimal totalPrice = seckillConfirmVO.getPrice().multiply(new BigDecimal(seckillSkuDTO.getNum()));
        seckillConfirmVO.setTotalPrice(totalPrice);
        // 计算运费，如果单笔订单金额大于等于包邮金额，则免运费，否则运费8元
        seckillConfirmVO.setFreight(totalPrice.compareTo(OrderConstant.FREE_FREIGHT_PRICE) >= 0 ? BigDecimal.ZERO
                : OrderConstant.FREIGHT);
        seckillConfirmVO.setPayPrice(seckillConfirmVO.getTotalPrice().add(seckillConfirmVO.getFreight()));
        return seckillConfirmVO;
    }

    /**
     * 提交秒杀订单
     * @param seckillOrderSubmitVO
     */
    @Override
    public OrderSubmitResponseVO submitSeckillOrder(SeckillOrderSubmitVO seckillOrderSubmitVO) {
        R r = memberFeignService.getReceiveAddressInfo(seckillOrderSubmitVO.getAddrId());
        if (r.getCode() == 0) {
            MemberReceiveAddressDTO receiveAddressInfo = r.getData("memberReceiveAddress",
                    new TypeReference<MemberReceiveAddressDTO>() {
                    });
            // 设置收货人信息
            OrderEntity order = new OrderEntity();
            org.springframework.beans.BeanUtils.copyProperties(receiveAddressInfo,order);
            order.setReceiverName(receiveAddressInfo.getName());
            order.setReceiverPhone(receiveAddressInfo.getPostCode());
            order.setReceiverProvince(receiveAddressInfo.getProvince());
            order.setReceiverCity(receiveAddressInfo.getCity());
            order.setReceiverRegion(receiveAddressInfo.getRegion());
            order.setReceiverDetailAddress(receiveAddressInfo.getDetailAddress());
            order.setOrderSn(seckillOrderSubmitVO.getOrderSn());
            OrderEntity oldOrder = this.getOrderByOrderSn(seckillOrderSubmitVO.getOrderSn());
            order.setId(oldOrder.getId());
            this.updateById(order);

            OrderSubmitResponseVO responseVO = new OrderSubmitResponseVO();
            responseVO.setOrder(oldOrder);
            responseVO.setCode(0);
            return responseVO;
        }
        return null;
    }

    /**
     * 构建秒杀订单基本数据
     *
     * @param orderSn
     * @return
     */
    private OrderEntity buildSeckillOrder(String orderSn) {
        // 获取用户选择的收货信息
        OrderEntity order = new OrderEntity();
        order.setOrderSn(orderSn);
        order.setAutoConfirmDay(7);
        order.setStatus(OrderConstant.OrderStatusEnum.TO_BE_PAID.getCode());

        order.setPromotionAmount(BigDecimal.ZERO);
        order.setIntegrationAmount(BigDecimal.ZERO);
        order.setCouponAmount(BigDecimal.ZERO);

        return order;
    }

    /**
     * 构建秒杀订单购物项
     *
     * @param seckillSkuDTO
     * @return
     */
    private OrderItemEntity buildSeckillOrderItem(SeckillSkuDTO seckillSkuDTO) {
        OrderItemEntity orderItem = new OrderItemEntity();
//        orderItem.setOrderId(0L);
        orderItem.setOrderSn(seckillSkuDTO.getOrderSn());

        // 商品的SPU信息
        R r = productFeignService.info(seckillSkuDTO.getSkuId());
        if (r.getCode() == 0) {
            SpuInfoDTO spuInfoDTO = r.getData(new TypeReference<SpuInfoDTO>() {
            });
            orderItem.setSpuId(spuInfoDTO.getId());
            orderItem.setSpuName(spuInfoDTO.getSpuName());
            orderItem.setSpuBrand(spuInfoDTO.getBrandId().toString());
            orderItem.setCategoryId(spuInfoDTO.getCategoryId());
        }


        // 商品的sku信息
        orderItem.setSkuId(seckillSkuDTO.getSkuId());
        orderItem.setSkuName(seckillSkuDTO.getTitle());
        orderItem.setSkuPic(seckillSkuDTO.getImage());
        orderItem.setSkuPrice(seckillSkuDTO.getPrice());
        orderItem.setSkuQuantity(seckillSkuDTO.getNum());
        // 将销售属性的List转换为String
        StringBuilder listStr = new StringBuilder();
        for (String attr : seckillSkuDTO.getSkuAttr()) {
            listStr.append(attr + ";");
        }
        orderItem.setSkuAttrsVals(listStr.toString());


        // 优惠信息[不做，直接赋0]
        orderItem.setCouponAmount(BigDecimal.ZERO);
        orderItem.setPromotionAmount(BigDecimal.ZERO);
        orderItem.setIntegrationAmount(BigDecimal.ZERO);

        // 最终价格：总额 - 所有优惠
        BigDecimal totalPrice = seckillSkuDTO.getPrice().multiply(new BigDecimal(seckillSkuDTO.getNum()));
        BigDecimal realAmount = totalPrice.subtract(orderItem.getCouponAmount())
                .subtract(orderItem.getPromotionAmount()).subtract(orderItem.getIntegrationAmount());
        orderItem.setRealAmount(realAmount);

        // 积分信息
        orderItem.setGiftIntegration(totalPrice.intValue());
        orderItem.setGiftGrowth(totalPrice.intValue());

        return orderItem;
    }


    /**
     * 创建订单（总）
     *
     * @return
     */
    private OrderCreateDTO createOrder() {
        OrderCreateDTO orderCreateDTO = new OrderCreateDTO();
        // 订单号
        String orderSn = IdWorker.getTimeId();

        // 1、构建订单基本数据
        OrderEntity order = buildOrder(orderSn);

        // 2、获取购物车中的所有选中购物项
        List<OrderItemEntity> orderItems = buildOrderItems(order.getOrderSn());

        // 3、计算价格等聚合信息
        computeOrderAggs(order, orderItems);

        orderCreateDTO.setOrder(order);
        orderCreateDTO.setOrderItems(orderItems);
        return orderCreateDTO;
    }

    /**
     * 计算订单最终的聚合信息
     *
     * @param order
     * @param orderItems
     */
    private void computeOrderAggs(OrderEntity order, List<OrderItemEntity> orderItems) {
        BigDecimal totalAmount = BigDecimal.ZERO;   // 订单总金额

        BigDecimal promotionAmount = BigDecimal.ZERO;   // 商品促销分解金额
        BigDecimal integrationAmount = BigDecimal.ZERO; // 积分优惠分解金额
        BigDecimal couponAmount = BigDecimal.ZERO;  // 优惠券优惠分解金额

        int integration = 0;    // 赠送积分
        int growth = 0; // 赠送成长值

        for (OrderItemEntity orderItem : orderItems) {
            totalAmount = totalAmount.add(orderItem.getRealAmount());
            promotionAmount = promotionAmount.add(orderItem.getPromotionAmount());
            integrationAmount = integrationAmount.add(orderItem.getIntegrationAmount());
            couponAmount = couponAmount.add(orderItem.getCouponAmount());
            integration += orderItem.getGiftIntegration();
            growth += orderItem.getGiftGrowth();
        }

        order.setTotalAmount(totalAmount);//订单总额
        order.setFreightAmount(totalAmount.compareTo(OrderConstant.FREE_FREIGHT_PRICE) >= 0 ? BigDecimal.ZERO
                : OrderConstant.FREIGHT);//运费金额
        order.setPayAmount(totalAmount.add(order.getFreightAmount()));//应付总额
        order.setPromotionAmount(promotionAmount);//促销优化金额（促销价、满减、阶梯价）
        order.setIntegrationAmount(integrationAmount);//积分抵扣金额
        order.setCouponAmount(couponAmount);//优惠券抵扣金额
        order.setIntegration(integration);//可以获得的积分
        order.setGrowth(growth);//可以获得的成长值
    }

    /**
     * 构建订单基本数据
     *
     * @param orderSn
     * @return
     */
    private OrderEntity buildOrder(String orderSn) {
        OrderSubmitVO orderSubmitVO = threadLocal.get();
        MemberResponseVo memberTO = LoginUserInterceptor.threadLocal.get();
        // 获取用户选择的收货信息
        R r = memberFeignService.getReceiveAddressInfo(orderSubmitVO.getAddrId());
        OrderEntity order = new OrderEntity();
        if (r.getCode() == 0) {
            MemberReceiveAddressDTO receiveAddressInfo = r.getData("memberReceiveAddress",
                    new TypeReference<MemberReceiveAddressDTO>() {
                    });
            // 设置收货人信息
//            order = OrderCovertBasic.INSTANCE.MemberReceiveAddressDTOToOrder(receiveAddressInfo);
            order.setReceiverName(receiveAddressInfo.getName());
            order.setReceiverPhone(receiveAddressInfo.getPhone());
            order.setReceiverPostCode(receiveAddressInfo.getPostCode());
            order.setReceiverProvince(receiveAddressInfo.getProvince());
            order.setReceiverRegion(receiveAddressInfo.getRegion());
            order.setReceiverDetailAddress(receiveAddressInfo.getDetailAddress());
            order.setOrderSn(orderSn);
            order.setAutoConfirmDay(7);//自动确认时间（天）
            order.setStatus(OrderConstant.OrderStatusEnum.TO_BE_PAID.getCode());

            order.setMemberId(memberTO.getId());
            order.setCreateTime(new Date());
        }
        return order;
    }

    /**
     * 根据购物车的选中项创建订单项
     *
     * @return
     */
    private List<OrderItemEntity> buildOrderItems(String orderSn) {
        // 这个方法会更新购物车中所有商品的最新价格，这也是下单流程中最后一次确定购物项的价格了
        List<OrderConfirmVO.OrderItemVO> OrderItemVOs = cartFeignService.getCurrentUserCartItems();
        if (CollectionUtils.isNotEmpty(OrderItemVOs)) {
            List<OrderItemEntity> orderItems = OrderItemVOs.stream().map(item -> {
                OrderItemEntity orderItem = buildOrderItem(item); //构建某一个订单购物项
                orderItem.setOrderSn(orderSn);
                return orderItem;
            }).collect(Collectors.toList());
            return orderItems;
        }
        return null;
    }

    /**
     * 构建某一个订单购物项
     *
     * @param orderItemVO
     * @return
     */
    private OrderItemEntity buildOrderItem(OrderConfirmVO.OrderItemVO orderItemVO) {
        OrderItemEntity orderItem = new OrderItemEntity();
//        orderItem.setOrderId(0L);

        // 商品的SPU信息
        R r = productFeignService.info(orderItemVO.getSkuId());
        if (r.getCode() == 0) {
            SpuInfoDTO spuInfoDTO = r.getData(new TypeReference<SpuInfoDTO>() {
            });
            orderItem.setSpuId(spuInfoDTO.getId());
            orderItem.setSpuName(spuInfoDTO.getSpuName());
            orderItem.setSpuBrand(spuInfoDTO.getBrandId().toString());
            orderItem.setCategoryId(spuInfoDTO.getCategoryId());
        }


        // 商品的sku信息
        orderItem.setSkuId(orderItemVO.getSkuId());
        orderItem.setSkuName(orderItemVO.getTitle());
        orderItem.setSkuPic(orderItemVO.getImage());
        orderItem.setSkuPrice(orderItemVO.getPrice());
        orderItem.setSkuQuantity(orderItemVO.getCount());

        // 将销售属性的List转换为String
        StringBuilder listStr = new StringBuilder();
        if(CollectionUtils.isNotEmpty(orderItemVO.getSkuAttr())){
            for (String attr : orderItemVO.getSkuAttr()) {
                listStr.append(attr + ";");
            }
            orderItem.setSkuAttrsVals(listStr.toString());
        }
        // 优惠信息[不做，直接赋0]
        orderItem.setCouponAmount(BigDecimal.ZERO);
        orderItem.setPromotionAmount(BigDecimal.ZERO);
        orderItem.setIntegrationAmount(BigDecimal.ZERO);

        // 最终价格：总额 - 所有优惠
        BigDecimal totalPrice = orderItemVO.getPrice().multiply(new BigDecimal(orderItemVO.getCount()));
        BigDecimal realAmount = totalPrice.subtract(orderItem.getCouponAmount())
                .subtract(orderItem.getPromotionAmount()).subtract(orderItem.getIntegrationAmount());
        orderItem.setRealAmount(realAmount);

        // 积分信息
        orderItem.setGiftIntegration(totalPrice.intValue());
        orderItem.setGiftGrowth(totalPrice.intValue());

        return orderItem;
    }

    /**
     * 保存订单、订单项
     *
     * @param orderCreateDTO
     */
    private void saveOrderAndItems(OrderCreateDTO orderCreateDTO) {
        this.save(orderCreateDTO.getOrder());
        for(OrderItemEntity itemEntity : orderCreateDTO.getOrderItems()){
            orderItemService.save(itemEntity);
        }
    }


    /**
     * 订单确认页：计算订单总额、应付价格
     *
     * @param orderConfirmVO
     */
    private void setAggs(OrderConfirmVO orderConfirmVO) {
        List<OrderConfirmVO.OrderItemVO> items = orderConfirmVO.getItems();
        BigDecimal totalPrice = BigDecimal.ZERO;
        Integer totalCount = 0;
        for (OrderConfirmVO.OrderItemVO item : items) {
            // 为避免redis数据库中的商品存放时间过长，导致价格变更，此处必须重新计算价格
            if (item != null) {
                totalPrice = totalPrice.add(item.getPrice().multiply(new BigDecimal(item.getCount())));
                totalCount += item.getCount();
            }
        }
        orderConfirmVO.setTotalPrice(totalPrice);
        orderConfirmVO.setTotalCount(totalCount);
        // 计算运费，如果单笔订单金额大于等于包邮金额，则免运费，否则运费8元
        orderConfirmVO.setFreight(totalPrice.compareTo(OrderConstant.FREE_FREIGHT_PRICE) >= 0 ? BigDecimal.ZERO
                : OrderConstant.FREIGHT);
        orderConfirmVO.setPayPrice(orderConfirmVO.getTotalPrice().add(orderConfirmVO.getFreight()));

    }
}