package com.atguigu.gulimall.order.web;

import com.alipay.api.AlipayApiException;
import com.atguigu.gulimall.order.config.AlipayTemplate;
import com.atguigu.gulimall.order.service.OrderService;
import com.atguigu.gulimall.order.vo.PayVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author meiyn
 * @date 2021/7/25 18:41
 */
@Controller
public class OrderPayController {
    @Resource
    private AlipayTemplate alipayTemplate;

    @Resource
    private OrderService orderService;

    /**
     * 支付宝支付
     * 1.将支付页让浏览器展示
     * 2.支付成功后，我们要调到订单列表页
     *
     * @param orderSn
     * @return
     */
    //produces = "text/html" : 告诉浏览器返回的直接是一个页面
    @GetMapping(value = "/order-pay/alipay", produces = "text/html")
    @ResponseBody
    public String alipay(@RequestParam String orderSn) throws AlipayApiException {
        PayVO payVO = orderService.getOrderPayInfo(orderSn);
        String routePage = alipayTemplate.pay(payVO);
        //支付宝返回的是一个页面，将此页面直接返回浏览器。
        return routePage;
    }
}
