package com.atguigu.gulimall.order.config;

import com.atguigu.gulimall.order.interceptor.LoginUserInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Administrator
 * @create 2022/2/21 10:54
 */
@Configuration
public class OrderWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * addInterceptor():添加自定义拦截器
         * addPathPatterns("/**")：添加项目里所有请求都调用此拦截器
         */
        registry.addInterceptor(new LoginUserInterceptor()).addPathPatterns("/**");
    }
}
