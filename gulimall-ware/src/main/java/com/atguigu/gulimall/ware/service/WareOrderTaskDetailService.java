package com.atguigu.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.WareOrderTaskDetailEntity;

import java.util.List;
import java.util.Map;

/**
 * 库存工作单
 *
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-23 15:33:18
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据库存工作单id查出已锁定的工作单详情
     * @param id
     * @return
     */
    List<WareOrderTaskDetailEntity> lockedListByTaskId(Long id);
}

