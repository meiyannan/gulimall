package com.atguigu.gulimall.ware.service;

import com.atguigu.common.dto.OrderDTO;
import com.atguigu.common.dto.WareLockStockDTO;
import com.atguigu.common.dto.mq.StockLockDTO;
import com.atguigu.common.to.SkuHasStockVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-23 15:33:18
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    /**
     * 判断是否有库存
     * @param skuIds
     * @return
     */
    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);

    /**
     * 锁定下单商品的库存
     * @param wareLockStockDTO
     * @return
     */
    void lockStock(WareLockStockDTO wareLockStockDTO);

    /**
     * 自动解锁库存
     * @param stockLockDTO
     */
    void unlockStock(StockLockDTO stockLockDTO);

    /**
     * 主动解锁库存
     * @param orderDTO
     */
    void unlockStock(OrderDTO orderDTO);

}

