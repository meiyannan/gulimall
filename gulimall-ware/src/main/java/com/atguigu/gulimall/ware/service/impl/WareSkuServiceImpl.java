package com.atguigu.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.contrsant.OrderConstant;
import com.atguigu.common.contrsant.WareConstant;
import com.atguigu.common.dto.OrderDTO;
import com.atguigu.common.dto.WareLockStockDTO;
import com.atguigu.common.dto.mq.StockLockDTO;
import com.atguigu.common.exception.Assert;
import com.atguigu.common.exception.BizCodeEnume;
import com.atguigu.common.to.SkuHasStockVo;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.atguigu.gulimall.ware.entity.WareOrderTaskEntity;
import com.atguigu.gulimall.ware.feign.OrderFeignService;
import com.atguigu.gulimall.ware.feign.ProductFeignService;
import com.atguigu.gulimall.ware.service.WareOrderTaskDetailService;
import com.atguigu.gulimall.ware.service.WareOrderTaskService;
import lombok.Data;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.ware.dao.WareSkuDao;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;
import com.atguigu.gulimall.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {


    @Autowired
    WareSkuDao wareSkuDao;

    @Autowired
    ProductFeignService productFeignService;

    @Resource
    private WareOrderTaskService wareOrderTaskService;

    @Resource
    private WareOrderTaskDetailService wareOrderTaskDetailService;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private OrderFeignService orderFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        /**
         * skuId: 1
         * wareId: 2
         */
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if(!StringUtils.isEmpty(skuId)){
            queryWrapper.eq("sku_id",skuId);
        }

        String wareId = (String) params.get("wareId");
        if(!StringUtils.isEmpty(wareId)){
            queryWrapper.eq("ware_id",wareId);
        }


        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    /**
     * 添加库存
     * @param skuId
     * @param wareId
     * @param skuNum
     */
    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        //1、判断如果还没有这个库存记录新增
        List<WareSkuEntity> entities = wareSkuDao.selectList(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));
        if(entities == null || entities.size() == 0){
            WareSkuEntity skuEntity = new WareSkuEntity();
            skuEntity.setSkuId(skuId);
            skuEntity.setStock(skuNum);
            skuEntity.setWareId(wareId);
            skuEntity.setStockLocked(0);
            //TODO 远程查询sku的名字，如果失败，整个事务无需回滚
            //1、自己catch异常
            //TODO 还可以用什么办法让异常出现以后不回滚？高级
            try {
                R info = productFeignService.info(skuId);
                Map<String,Object> data = (Map<String, Object>) info.get("skuInfo");

                if(info.getCode() == 0){
                    skuEntity.setSkuName((String) data.get("skuName"));
                }
            }catch (Exception e){

            }


            wareSkuDao.insert(skuEntity);
        }else{
            wareSkuDao.addStock(skuId,wareId,skuNum);
        }

    }

    /**
     * 判断是否有库存
     * @param skuIds
     * @return
     */
    @Override
    public List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds) {

        List<SkuHasStockVo> skuHasStockVos = skuIds.stream().map(item -> {
            Long count = this.baseMapper.getSkuStock(item);
            SkuHasStockVo skuHasStockVo = new SkuHasStockVo();
            skuHasStockVo.setSkuId(item);
            skuHasStockVo.setHasStock(count == null?false:count > 0);
            return skuHasStockVo;
        }).collect(Collectors.toList());
        return skuHasStockVos;
    }


    /**
     * 锁定下单商品的库存
     * @param wareLockStockDTO
     * @return
     */
    @Transactional
    @Override
    public void lockStock(WareLockStockDTO wareLockStockDTO) {
        // 保存库存工作单，方便追溯源头
        WareOrderTaskEntity wareOrderTask = new WareOrderTaskEntity();
        wareOrderTask.setOrderSn(wareLockStockDTO.getOrderSn());
        wareOrderTask.setCreateTime(new Date());
        wareOrderTaskService.save(wareOrderTask);

        // 遍历所有要购买的商品，查询库存中满足这些商品数量的所有仓库
        List<WareHasStock> wareHasStocks = wareLockStockDTO.getItems().stream().map(item -> {
            List<Long> wareIds = wareSkuDao.wareIdList(item.getSkuId(), item.getSkuQuantity());

            // 如果有一个商品查不出符合条件的仓库，就直接抛异常，下单失败
            Assert.notEmptyList(wareIds, BizCodeEnume.NO_STOCK_EXCEPTION);

            WareHasStock wareHasStock = new WareHasStock();
            wareHasStock.setSkuId(item.getSkuId());
            wareHasStock.setWareIds(wareIds);
            wareHasStock.setSkuQuantity(item.getSkuQuantity());
            return wareHasStock;
        }).collect(Collectors.toList());

        // 走到这，就说明所有的商品都有库存，可以开始锁库存了
        for (WareHasStock item : wareHasStocks) {
            boolean stockLock = false; // 是否锁定成功

            for (Long wareId : item.getWareIds()) {
                int count = wareSkuDao.lockStock(item.getSkuId(), wareId, item.getSkuQuantity());
                /*
                    1、如果当前商品都锁定成功，就将当前商品锁定的工作单记录发给MQ
                    2、锁定失败。前面保存的工作单信息就回滚了。
                 */
                if (count == 1) {
                    // 保存库存工作单详情 并且往MQ中发送库存锁定消息-
                    saveWareOrderTaskDetail(wareOrderTask, item, wareId);

                    // 说明锁成功了，直接跳出循环，开始锁下一个商品
                    stockLock = true;
                    break;
                } else {
                    stockLock = false;
                }
            }

            // 这里判断stockLock，一旦为false，就说明当前商品在所有仓库中都锁定失败了，直接抛异常，下单失败
            Assert.isTrue(stockLock, BizCodeEnume.NO_STOCK_EXCEPTION);
        }
    }

    /**
     * 自动解锁库存
     * @param stockLockDTO
     */
    @Transactional
    @Override
    public void unlockStock(StockLockDTO stockLockDTO) {
        // 库存工作单
        WareOrderTaskEntity wareOrderTask = wareOrderTaskService.getById(stockLockDTO.getId());
        if (wareOrderTask != null) {
            // 库存工作单详情
            WareOrderTaskDetailEntity orderTaskDetail = wareOrderTaskDetailService.getById(stockLockDTO
                    .getStockLockDetailDTO().getId());

            String orderSn = wareOrderTask.getOrderSn();
            R r = orderFeignService.infoByOrderSn(orderSn);
            // 远程服务调用失败
            Assert.isTrue(r.getCode() == 0, BizCodeEnume.FEIGN_SERVICE_EXCEPTION);
            // 订单信息
            OrderDTO orderDTO = r.getData(new TypeReference<OrderDTO>() {
            });
            // 查出的订单数据为空、订单状态已取消
            if (orderDTO == null || orderDTO.getStatus() == OrderConstant.OrderStatusEnum.CANCELLED.getCode()) {
                // 库存工作单详情的状态为已锁定
                if (orderTaskDetail.getLockStatus() == WareConstant.LockStatusEnum.LOCKED.getCode()) {
                    finalUnlock(orderTaskDetail);
                }
            }

        }
    }

    /**
     * 主动解锁库存
     * @param orderDTO
     */
    @Transactional
    @Override
    public void unlockStock(OrderDTO orderDTO) {
        WareOrderTaskEntity task = wareOrderTaskService.infoByOrderSn(orderDTO.getOrderSn());
        // 根据库存工作单id查出所有已锁定的工作单详情
        List<WareOrderTaskDetailEntity> details = wareOrderTaskDetailService.lockedListByTaskId(task.getId());
        for (WareOrderTaskDetailEntity detail : details) {
            // 彻底解锁
            finalUnlock(detail);
        }
    }

    /**
     * 最终解锁
     *
     * @param orderTaskDetail
     */
    private void finalUnlock(WareOrderTaskDetailEntity orderTaskDetail) {
        // 解锁库存
        wareSkuDao.unlockStock(orderTaskDetail.getSkuId(), orderTaskDetail.getWareId(),
                orderTaskDetail.getSkuNum());
        // 将库存工作单详情的状态改为已解锁
        WareOrderTaskDetailEntity finalTaskDetail = new WareOrderTaskDetailEntity();
        finalTaskDetail.setLockStatus(WareConstant.LockStatusEnum.UNLOCKED.getCode());
        finalTaskDetail.setId(orderTaskDetail.getId());
        wareOrderTaskDetailService.updateById(finalTaskDetail);
    }

    /**
     * 保存库存工作单详情
     *
     * @param wareOrderTask
     * @param item
     * @param wareId
     */
    private void saveWareOrderTaskDetail(WareOrderTaskEntity wareOrderTask, WareHasStock item, Long wareId) {
        WareOrderTaskDetailEntity wareOrderTaskDetail = new WareOrderTaskDetailEntity();
        wareOrderTaskDetail.setSkuId(item.getSkuId());
        wareOrderTaskDetail.setSkuNum(item.getSkuQuantity());
        wareOrderTaskDetail.setTaskId(wareOrderTask.getId());
        wareOrderTaskDetail.setWareId(wareId);
        wareOrderTaskDetail.setLockStatus(1);
        wareOrderTaskDetailService.save(wareOrderTaskDetail);

        // 发消息保存库存工作单详情的数据，防止回滚之后没有数据
        StockLockDTO stockLockDTO = new StockLockDTO();
        stockLockDTO.setId(wareOrderTask.getId());

        StockLockDTO.StockLockDetailDTO stockLockDetailDTO = new StockLockDTO.StockLockDetailDTO();
        BeanUtils.copyProperties(wareOrderTaskDetail,stockLockDetailDTO);
        stockLockDTO.setStockLockDetailDTO(stockLockDetailDTO);
        rabbitTemplate.convertAndSend(WareConstant.STOCK_EVENT_EXCHANGE, WareConstant.STOCK_LOCKED_ROUTING_KEY,
                stockLockDTO);
    }

    @Data
    class WareHasStock {
        private Long skuId;
        private List<Long> wareIds;
        private Integer skuQuantity;
    }

}