package com.atguigu.gulimall.seckill.controller;

import com.atguigu.common.dto.SeckillSkuRedisDTO;
import com.atguigu.common.dto.mq.SeckillSkuDTO;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.seckill.service.SeckillService;
import com.atguigu.gulimall.seckill.vo.SeckillSkuRedisTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author meiyn
 * @date 2021/7/28 8:38
 */
@Slf4j
@Controller
public class SeckillController {
    @Resource
    private SeckillService seckillService;

    /**
     * 获取当前时间应该秒杀的商品
     *
     * @return
     */
    @GetMapping("/web/seckill/current-seckill-skus")
    @ResponseBody
    public R getCurrentSeckillSkus() {
        log.info("获取当前时间应该秒杀的商品.....正在执行");
        List<SeckillSkuRedisDTO> seckillSkuRedisDTOs = seckillService.getCurrentSeckillSkus();
        return R.ok().setData(seckillSkuRedisDTOs);
    }

    /**
     * 根据skuId获取对应的秒杀信息
     *
     * @return
     */
    @GetMapping("/web/seckill/seckill-sku-relation")
    public R getSeckillSkuRelation(@RequestParam("skuId") Long skuId) {
        SeckillSkuRedisDTO relationDTO = seckillService.getSeckillSkuRelation(skuId);
        return R.ok().setData(relationDTO);
    }

    /**
     * 根据skuId查询商品是否参加秒杀活动
     * @param skuId
     * @return
     */
    @GetMapping(value = "/sku/seckill/{skuId}")
    @ResponseBody
    public R getSkuSeckilInfo(@PathVariable("skuId") Long skuId) {

        SeckillSkuRedisTo to = seckillService.getSkuSeckilInfo(skuId);

        return R.ok().setData(to);
    }


    /**
     * 秒杀
     *
     * @return
     */
    @GetMapping("/web/seckill/kill")
    public String kill(@RequestParam("killId") String killId,
                       @RequestParam("randomCode") String randomCode,
                       @RequestParam("num") Integer num,
                       Model model) {
        SeckillSkuDTO seckillSkuDTO = seckillService.kill(killId, randomCode, num);
        model.addAttribute("seckillSkuInfo", seckillSkuDTO);
        return "seckill-result";
    }

}
