package com.atguigu.gulimall.seckill.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 优惠系统的远程服务
 *
 * @author meiyn
 * @date 2021/7/27 9:13
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {
    /**
     * 查出最近3天的秒杀场次、以及商品
     * @return
     */
    @GetMapping("coupon/seckillsession/list-latest-3days")
    R listByLatest3Days();
}
