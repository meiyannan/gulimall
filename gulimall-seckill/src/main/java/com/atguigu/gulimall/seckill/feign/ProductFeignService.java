package com.atguigu.gulimall.seckill.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author meiyn
 * @date 2021/7/18 19:21
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {
    @GetMapping("product/skuinfo/info/{skuId}")
    R skuInfo(@PathVariable("skuId") Long skuId);

    /**
     * 根据skuId获取拼接后的值
     * 格式 -> 销售属性：属性值
     * @param skuId
     * @return
     */
    @GetMapping("/product/skusaleattrvalue/stringList/{skuId}")
    R getSaleAttrNameAndValues(@PathVariable("skuId") Long skuId);
}
