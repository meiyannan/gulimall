package com.atguigu.gulimall.seckill.service;

import com.atguigu.common.dto.SeckillSkuRedisDTO;
import com.atguigu.common.dto.mq.SeckillSkuDTO;
import com.atguigu.gulimall.seckill.vo.SeckillSkuRedisTo;

import java.util.List;

/**
 * @author UnityAlvin
 * @date 2021/7/27 9:12
 */
public interface SeckillService {
    /**
     * 上架最近3天的秒杀活动、以及商品
     */
    void uploadSeckillSkuLatest3Days();

    /**
     * 获取当前时间可以秒杀的商品
     * @return
     */
    List<SeckillSkuRedisDTO> getCurrentSeckillSkus();

    /**
     * 根据skuId获取对应的秒杀信息
     * @return
     */
    SeckillSkuRedisDTO getSeckillSkuRelation(Long skuId);

    /**
     * 根据skuId查询商品是否参加秒杀活动
     * @param skuId
     * @return
     */
    SeckillSkuRedisTo getSkuSeckilInfo(Long skuId);

    /**
     * 秒杀
     * @param killId
     * @param randomCode
     * @param num
     * @return
     */
    SeckillSkuDTO kill(String killId, String randomCode, Integer num);
//
//    /**
//     * 释放秒杀订单的库存
//     * @param seckillSkuDTO
//     */
//    void releaseStock(SeckillSkuDTO seckillSkuDTO);
}
