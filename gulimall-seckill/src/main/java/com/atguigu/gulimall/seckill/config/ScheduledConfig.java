package com.atguigu.gulimall.seckill.config;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 秒杀任务配置
 * @author meiyn
 * @date 2021/7/27 9:05
 */
@EnableAsync    // 开启异步任务功能
@EnableScheduling // 开启定时功能
public class ScheduledConfig {
}
