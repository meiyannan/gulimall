package com.atguigu.gulimall.auth.config;

import com.alibaba.csp.sentinel.adapter.servlet.callback.WebCallbackManager;
import com.alibaba.fastjson.JSON;
import com.atguigu.common.exception.BizCodeEnume;
import com.atguigu.common.utils.R;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SeckillSentinelConfig {
    /**
     * 自定义秒杀流控响应(熔断请求的响应)
     */
    public SeckillSentinelConfig(){
        WebCallbackManager.setUrlBlockHandler((request, response, e) ->{
            R error = R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(), BizCodeEnume.TO_MANY_REQUEST.getMsg());
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            response.getWriter().write(JSON.toJSONString(error));
        });
    }
}
