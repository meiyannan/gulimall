package com.atguigu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.coupon.entity.SeckillSkuRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 秒杀活动商品关联
 *
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-23 14:51:15
 */
public interface SeckillSkuRelationService extends IService<SeckillSkuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据秒杀活动场次id查询对应的秒杀商品
     * @param id
     * @return
     */
    List<SeckillSkuRelationEntity> listByPromotionSessionId(Long id);
}

