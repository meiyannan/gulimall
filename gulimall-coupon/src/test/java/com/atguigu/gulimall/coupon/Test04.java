package com.atguigu.gulimall.coupon;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

@Slf4j
public class Test04 {

    public static void main(String[] args) throws Exception {
        String url = "http://10.8.7.90:8081/oapi/archive/supplierDataSync";
        String body = "{\n" +
                "\t\"agencyCreditCode\": \"93100000TFG1RQ5W7X\",\n" +
                "\t\"agencyCustCode\": \"ZS_DLS20210721N0322\",\n" +
                "\t\"agencyCustName\": \"湖南猎豹汽车股份有限公司1\",\n" +
                "\t\"pkCountry\": \"CN\",\n" +
                "\t\"pkFormat\": \"ZH-CN\",\n" +
                "\t\"pkGroup\": \"01\",\n" +
                "\t\"pkOrg\": \"01\",\n" +
                "\t\"pkSupplierClass\": \"0209\",\n" +
                "\t\"pkTimezone\": \"P0800\"\n" +
                "}";
        Test04 test = new Test04();
        test.post(url,body);
    }


    private static int SOCKET_TIMEOUT = 300 * 1000;

    private static int CONNECT_TIMEOUT = 300 * 1000;


    /**
     *
     * @param url 请求回调地址
     * @param body 请求体
     * @return 返回报文
     */
    public static String post(String url,String body){

        // channelId appKey appSecret 为配置项 测试环境&生产环境待提供
        String channelId = "A0001";
        String appKey = "zu1d0hek";
        String appSecret = "ecj22sjndcf0usbr";

        CloseableHttpClient httpClient = null;
        String retMsg = null;
        try {
            httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            StringEntity strEntity = new StringEntity(body,"UTF-8");
            httpPost.setEntity(strEntity);
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIMEOUT)
                    .setConnectTimeout(CONNECT_TIMEOUT).build();
            httpPost.setConfig(requestConfig);
            String nonce = RandomUtil.randomString(16);
            long timestamp = System.currentTimeMillis();
            StringBuilder sourceBuilder = new StringBuilder();
            sourceBuilder.append("channelId=")
                    .append(channelId)
                    .append("&timestamp=")
                    .append(timestamp)
                    .append("&nonce=")
                    .append(nonce)
                    .append("&appKey=")
                    .append(appKey)
                    .append("&data=")
                    .append(body);
            log.info("source:{}",sourceBuilder.toString());
            byte[] serverSign = new HmacUtils(HmacAlgorithms.HMAC_SHA_256,appSecret).hmac(sourceBuilder.toString());
            String signature = Base64.encode(serverSign);
            httpPost.setHeader("accept", "*/*");
            httpPost.setHeader("connection", "Keep-Alive");
            httpPost.setHeader("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("XCL-Id", channelId);
            httpPost.setHeader("XCL-Timestamp", String.valueOf(timestamp));
            httpPost.setHeader("XCL-Nonce", nonce);
            httpPost.setHeader("XCL-TxnSn", IdUtil.simpleUUID());
            httpPost.setHeader("XCL-Signature", signature);
            log.info("请求地址:{}",url);
            log.info("请求时间戳:{}",timestamp);
            log.info("请求body:{}",body);
            log.info(signature);
            CloseableHttpResponse response = httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode > 300) {
                throw new Exception("partner server return error, code=" + statusCode);
            }
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                retMsg = EntityUtils.toString(entity, "UTF-8");
                log.info("响应结果:{}",retMsg);
            }
        } catch (Exception e) {
            log.error("接口调用异常",e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    log.error("请求异常");
                }
            }
        }
        return retMsg;
    }


}
