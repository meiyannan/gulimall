package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: meiyn
 * @createTime: 2021-10-11 11:53
 **/

@Data
public class AttrValueWithSkuIdVo {

    private String attrValue;

    private String skuIds;

}
