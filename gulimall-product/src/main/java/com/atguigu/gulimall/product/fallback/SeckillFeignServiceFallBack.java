package com.atguigu.gulimall.product.fallback;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.feigin.SeckillFeignService;
import org.springframework.stereotype.Component;
import com.atguigu.common.exception.BizCodeEnume;


/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: meiyn
 * @createTime: 2021-10-11 11:53
 **/

@Component
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckilInfo(Long skuId) {
        return R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(),BizCodeEnume.TO_MANY_REQUEST.getMsg());
    }
}
