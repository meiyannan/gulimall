package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author meiyannan
 * @email m1962213002.@163.com
 * @date 2021-04-22 15:16:58
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
