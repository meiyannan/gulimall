package com.atguigu.gulimall.product.feigin;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.fallback.SeckillFeignServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: meiyn
 * @createTime: 2021-10-11 11:53
 **/

@FeignClient(value = "gulimall-seckill",fallback = SeckillFeignServiceFallBack.class)
public interface SeckillFeignService {

    /**
     * 根据skuId查询商品是否参加秒杀活动
     * @param skuId
     * @return
     */
//    @GetMapping(value = "/web/seckill/seckill-sku-relation")
//    R getSkuSeckilInfo(@RequestParam("skuId") Long skuId);

    @GetMapping(value = "/sku/seckill/{skuId}")
    R getSkuSeckilInfo(@PathVariable("skuId") Long skuId);

}
